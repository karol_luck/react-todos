import React, { Component } from 'react';
import Todos from "./components/todos";
import LoginBar from "./components/login-bar";
import MainContainer from "./components/layout/main-container";
import {PanelHeader, PanelContent} from "./components/layout/panel";
import './App.css';
import {addTodo, completeTodo, fetchTodos, removeTodo} from "./actions/todosActions";
import {connect} from "react-redux";
import ListFilter from "./components/list-filter";
import {selectedTodosSelector, filteredTodosCountSelector} from "./selectors/todosSelectors";
import Pagination from "./components/pagination";
import {changePage, setPage} from "./actions/paginationActions";
import {filterOwn, filterTitle} from "./actions/filtersActions";
import TodoAdd from "./components/todo-add";

class App extends Component {
  componentDidMount() {
    this.props.fetchTodos();
  }

  render() {
    return (
      <div>
        <LoginBar userId={this.props.userId} />
        <MainContainer>
          <PanelHeader>Things to do</PanelHeader>
          <PanelContent style={{borderBottom: 0}}>
            <ListFilter filterTitle={this.props.filterTitle} filterOwn={this.props.filterOwn}/>
          </PanelContent>
          {!this.props.loadingTodos && <div>
            <PanelContent style={{borderBottom: 0}}>
              <Todos list={this.props.todosList} remove={this.props.removeTodo} complete={this.props.completeTodo} />
            </PanelContent>
            <PanelContent style={{borderBottom: 0}}>
              <TodoAdd userId={this.props.userId} addTodo={this.props.addTodo} />
            </PanelContent>
            <PanelContent>
              <Pagination
                changePage={this.props.changePage}
                pagination={this.props.pagination}
                itemsCount={this.props.todosCount}
              />
            </PanelContent>
          </div>}
          {this.props.loadingTodos && <PanelContent style={{padding: 10}}>
            Loading TODO tasks...
          </PanelContent>}
        </MainContainer>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loadingTodos: state.todos.isLoading,
    todosList: selectedTodosSelector(state),
    todosCount: filteredTodosCountSelector(state),
    pagination: state.pagination,
    userId: state.user.id
  }
};

const mapDispatchToProps = dispatch => ({
  fetchTodos: () => {
    dispatch(fetchTodos());
    dispatch(setPage(0));
  },
  changePage: (diff) => {
    dispatch(changePage(diff));
  },
  filterTitle: (title) => {
    dispatch(filterTitle(title));
    dispatch(setPage(0));
  },
  filterOwn: (enabled) => {
    dispatch(filterOwn(enabled));
    dispatch(setPage(0));
  },
  removeTodo: (id) => {
    dispatch(removeTodo(id));
  },
  completeTodo: (id) => {
    dispatch(completeTodo(id));
  },
  addTodo: (userId, title) => {
    dispatch(addTodo(userId, title));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
