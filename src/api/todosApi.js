import config from '../config/config';

export function todosGetRequest() {
  return fetch(`${config.apiUrl}todos`).then(data => data.json());
}
