import styled, {css} from "styled-components";

export const ListContainer = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;
`;

export const ListItem = styled.li`
  display: block;
  padding: 10px;
  transition: background-color 0.2s ease-in-out;
  
  &:nth-child(2n) {
    background-color: #E7F5FF;
  }
  
  &:hover {
    background-color: #C0DBFF;
    text-shadow: 0px 0px #000;
  }

  span.content {
    ${props => props.completed && css`
      color: green;
      display: block;
      text-decoration: line-through;
    `}
  }
`;
