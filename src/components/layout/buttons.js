import styled from 'styled-components';

export const ActionButton = styled.button`
  border: 0;
  border-radius: 2px;
  cursor: pointer;
`;
