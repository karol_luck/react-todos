import styled from 'styled-components';
import layoutConfig from '../../config/layout';

export const PanelHeader = styled.div`
  padding: 10px;
  background-color: ${layoutConfig.primaryColor};
  font-size: 22px;
  color: white;
`;

export const PanelContent = styled.div`
  border: 1px solid ${layoutConfig.primaryColor};
`;
