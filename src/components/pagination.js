import React from 'react';
import styled from "styled-components";
import {ActionButton} from "./layout/buttons";

const PaginationWrapper = styled.div`
  padding: 10px;
`;

const PageButton = styled(ActionButton)`
  margin: 0 5px;
  padding: 5px;
`;

export default class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.componentWillReceiveProps(props);
  }

  componentWillReceiveProps(nextProps) {
    this.pagesCount = Math.ceil(nextProps.itemsCount / nextProps.pagination.perPage);
  }

  render() {
    return <PaginationWrapper>
      Page:
      <PageButton disabled={this.props.pagination.page < 1} onClick={() => this.props.changePage(-1)}>Previous</PageButton>
      ({this.props.pagination.page + 1} of {this.pagesCount})
      <PageButton disabled={this.props.pagination.page + 1 >= this.pagesCount} onClick={() => this.props.changePage(1)}>Next</PageButton>
    </PaginationWrapper>;
  }
}
