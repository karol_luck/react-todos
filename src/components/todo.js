import React from 'react';
import {ListItem} from "./layout/list";
import TodoActions from "./todo-actions";

export default function Todo(props) {
  return <ListItem style={{display: 'flex'}} completed={props.data.completed}>
    <span className="content" style={{flex: 1}}>{props.data.title} [USER:{props.data.userId}]</span>
    <TodoActions data={props.data} remove={props.remove} complete={props.complete} />
  </ListItem>;
}
