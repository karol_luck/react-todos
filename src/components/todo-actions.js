import React from 'react';
import styled from 'styled-components';
import {ActionButton} from "./layout/buttons";

const CompleteButton = styled(ActionButton)`
  background-color: #84e482;
`;

const RemoveButton = styled(ActionButton)`
  background-color: #e48282;
`;

export default class TodoActions extends React.Component {
  render() {
    if (this.props.data.completed) {
      return <RemoveButton onClick={() => this.props.remove(this.props.data.id)}>Remove from list</RemoveButton>;
    } else {
      return <CompleteButton onClick={() => this.props.complete(this.props.data.id)}>Mark as complete</CompleteButton>;
    }
  }
}
