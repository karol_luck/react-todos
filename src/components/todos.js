import React from 'react';
import Todo from "./todo";
import {ListContainer} from "./layout/list";

export default class Todos extends React.Component {
  render() {
    return <ListContainer>
      {this.props.list.map(todo => <Todo data={todo} key={todo.id} remove={this.props.remove} complete={this.props.complete} />)}
    </ListContainer>;
  }
}
