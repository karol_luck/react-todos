import React from 'react';
import styled from 'styled-components';
import layoutConfig from '../config/layout';

const Bar = styled.div`
    width: 100%;
    height: 40px;
    background-color: ${layoutConfig.primaryColor};
    color: white;
    padding: 10px;
    margin-bottom: 20px;
    box-sizing: border-box;
`;

export default function LoginBar(props) {
    return <Bar>Zalogowano jako użytkownik o ID: {props.userId}</Bar>;
}
