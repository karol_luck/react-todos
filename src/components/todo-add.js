import React from 'react';
import styled from "styled-components";

const Input = styled.input`
  flex: 1;
  height: 30px;
  padding: 0 5px;
  font-size: 16px;
`;

const AddTitle = styled.span`
  line-height: 30px;
  padding: 0 10px;
`;

const AddButton = styled.button`
  border: 0;
  background-color: #84e482;
  padding: 0 10px;
  cursor: pointer;
`;

export default class TodoAdd extends React.Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }

  add = () => {
    if (!this.inputRef.current.value) {
      alert('Type new task name first...');
      return;
    }
    this.props.addTodo(this.props.userId, this.inputRef.current.value);
    this.inputRef.current.value = '';
  };

  render() {
    return <div style={{display: 'flex'}}>
      <AddTitle>Add new task: </AddTitle>
      <Input innerRef={this.inputRef} placeholder="Task name..." onChange={this.inputChanged} />
      <AddButton onClick={this.add}>Add +</AddButton>
    </div>;
  }
}
