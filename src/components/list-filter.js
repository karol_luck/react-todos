import React from 'react';
import styled from "styled-components";

const InputFilter = styled.input`
  flex: 1;
  height: 30px;
  padding: 0 5px;
  font-size: 16px;
`;

const ToggleLabel = styled.label`
  cursor: pointer;
  line-height: 30px;
  padding: 0 10px;
  font-size: 16px;
`;

export default class ListFilter extends React.Component {
  inputChanged = (e) => {
    this.props.filterTitle(e.target.value);
  };

  checkboxChanged = (e) => {
    this.props.filterOwn(e.target.checked);
  };

  render() {
    return <div style={{display: 'flex'}}>
      <InputFilter placeholder="Type here to filter tasks..." onChange={this.inputChanged} />
      <ToggleLabel>Show only my tasks: <input type="checkbox" onChange={this.checkboxChanged} /></ToggleLabel>
    </div>;
  }
}
