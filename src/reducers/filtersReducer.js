import initialState from "./initialState";
import {FILTER_OWN, FILTER_TITLE} from "../actions/filtersActions";

export default function filtersReducer(state = initialState.filters, action) {
  switch(action.type) {
    case FILTER_TITLE:
      return {
        ...state,
        title: action.title || null
      };
    case FILTER_OWN:
      return {
        ...state,
        onlyOwn: action.enabled
      };
    default:
      return state;
  }
}
