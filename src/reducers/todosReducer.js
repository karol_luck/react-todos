import initialState from './initialState';
import * as types from '../actions/todosActions';

export default function todosReducer(state = initialState.todos, action) {
  switch(action.type) {
    case types.ADD_TODO:
      return {
        ...state,
        list: [{
          userId: action.userId,
          id: state.lastId + 1,
          title: action.title
        }, ...state.list],
        lastId: state.lastId + 1
      };
    case types.REMOVE_TODO:
      return {
        ...state,
        list: state.list.filter(todo => todo.id !== action.id)
      };
    case types.FETCH_TODOS:
      return {
        ...state,
        isLoading: true
      };
    case types.COMPLETE_TODO:
      return {
        ...state,
        list: state.list.map(todo => todo.id !== action.id ? todo : {...todo, completed: true})
      };
    case types.RECEIVED_TODOS:
      return {
        ...state,
        list: action.list,
        lastId: action.list.reduce((maxId, next) => maxId < next.id ? next.id : maxId, 0),
        isLoading: false
      };
    default:
      return state;
  }
}
