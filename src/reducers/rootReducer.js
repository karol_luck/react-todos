import { combineReducers } from 'redux';
import todosReducer from "./todosReducer";
import userReducer from "./userReducer";
import paginationReducer from "./paginationReducer";
import filtersReducer from "./filtersReducer";

export default combineReducers({
  todos: todosReducer,
  user: userReducer,
  pagination: paginationReducer,
  filters: filtersReducer
})
