export default {
  todos: {
    list: [],
    lastId: 0,
    isLoading: false
  },
  filters: {
    title: null,
    onlyOwn: false
  },
  pagination: {
    perPage: 10,
    page: 0
  },
  user: {
    id: 2
  }
}
