import initialState from "./initialState";
import * as types from "../actions/paginationActions";

export default function paginationReducer(state = initialState.pagination, action) {
  switch(action.type) {
    case types.CHANGE_PAGE:
      return {
        ...state,
        page: state.page + action.diff
      };
    case types.SET_PAGE:
      return {
        ...state,
        page: action.pageIndex
      };
    default:
      return state;
  }
}
