import { createSelector } from 'reselect';

const todosList = state => state.todos.list;
const filters = state => state.filters;
const pagination = state => state.pagination;
const userId = state => state.user.id;

export const filteredTodosSelector = createSelector(
  todosList,
  filters,
  userId,
  (todos, filters, userId) => {
    const filtersFunc = [];
    if (filters.onlyOwn) {
      filtersFunc.push(todo => todo.userId === userId);
    }
    if (filters.title !== null) {
      filtersFunc.push(todo => todo.title.indexOf(filters.title) >= 0);
    }

    return !filtersFunc.length ? todos : todos.filter(todo => {
      return filtersFunc.every(filterFunc => filterFunc(todo));
    });
  }
);

export const selectedTodosSelector = createSelector(
  filteredTodosSelector,
  pagination,
  (todos, pagination) => {
    return todos.slice(pagination.page * pagination.perPage, pagination.page * pagination.perPage + pagination.perPage);
  }
);

export const filteredTodosCountSelector = createSelector(
  filteredTodosSelector,
  filteredTodos => filteredTodos.length
);
