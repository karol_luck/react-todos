export const FILTER_TITLE = 'FILTER_TITLE';
export const FILTER_OWN = 'FILTER_OWN';

export const filterTitle = (title) => ({
  title,
  type: FILTER_TITLE
});

export const filterOwn = (enabled) => ({
  enabled,
  type: FILTER_OWN
});
