export const CHANGE_PAGE = 'CHANGE_PAGE';
export const SET_PAGE = 'SET_PAGE';

export const changePage = (diff) => ({
  diff,
  type: CHANGE_PAGE
});

export const setPage = (pageIndex) => ({
  pageIndex,
  type: SET_PAGE
});
