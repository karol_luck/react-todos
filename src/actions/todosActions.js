export const ADD_TODO = 'ADD_TODO';
export const COMPLETE_TODO = 'COMPLETE_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const FETCH_TODOS = 'FETCH_TODOS';
export const CREATE_TODO = 'CREATE_TODO';
export const RECEIVED_TODOS = 'RECEIVED_TODOS';

export const fetchTodos = () => ({
  type: FETCH_TODOS
});

export const addTodo = (userId, title) => ({
  userId,
  title,
  type: ADD_TODO
});

export const completeTodo = id => ({
  id,
  type: COMPLETE_TODO
});

export const removeTodo = id => ({
  id,
  type: REMOVE_TODO
});
