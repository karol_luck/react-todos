import {takeLatest} from 'redux-saga/effects'
import { call, put } from 'redux-saga/effects'
import * as types from "../actions/todosActions";
import {todosGetRequest} from "../api/todosApi";

function* fetchTodos() {
  try {
    const response = yield call(todosGetRequest);
    yield put({type: types.RECEIVED_TODOS, list: response})
  } catch (error) {
    yield put({type: "REQUEST_FAILED", error})
  }
}

export default function* rootSaga() {
  yield takeLatest(types.FETCH_TODOS, fetchTodos);
}
